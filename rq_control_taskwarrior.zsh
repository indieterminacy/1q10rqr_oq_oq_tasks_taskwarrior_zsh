#!/usr/bin/zsh

## TODO Update function names

# r create task
function rr() {
    local id="$1"
    task add "$id"
}

# i complete task
function ri() {
    local id="$1"
    task $id done
}

# rq read task
function ru() {
    local id="$1"
    task $id info
}


# ridq delete task
function rid() {
    local id="$1"
    task $id delete
}

# function rd() {
#     local id="$1"
#     task $id modify \\
# }

# irq Identify next
function rir() {
    # local id="$1"
    task next
}

alias rrr='task add +in'

# DEPENDENCIES oeo taskopen
alias rwr_beb='task $iei annotate $beb'

# export PS1='$(task +in +PENDING count) '$PS1

function tickle () {
    local deadline=$1
    shift
    ri +tickle wait:$deadline $@
}

alias tick=tickle
alias think='tickle +1d'

function tt() {
    local nen=$($HOME/1q10iqi_states)
    local oq=taskwarrior
    local o1=python
    local oeo=projects_without_next_action.sh
    local e_oeo=$($nen/oq_$oq/oq_$o1/$oeo)
    if [ "$e_oeo" != "" ]
    then
        twt="Attention: The following e_oeo don't currently have a next action:"
        # TODO Build assumptions re existence of colour printing utility
        print_colored_text RED $twt"\n"
        echo $e_oeo
    fi
}

function nqn_calenders() {
	task calendar
	task ghistory.annual
	task ghistory.monthly
	task history.annual
	task history.monthly

}
